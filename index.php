<!DOCTYPE HTML>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>test</title>
  <script defer src="app/bundle.js"></script>
 </head>
 <body>
  <h1 id="main-tag">This isn't the text you are looking for!</h1>
  <p id="slave-tag">More text for testing</p>
  <p id="slave-tag">One more string</p>
  <p id="slave-tag">One more string</p>
  <p id="slave-tag">One more string</p>
 </body>
</html>